from django.contrib import admin
from .models import TodoList, TodoItem

# STEP 2: Register your models here.
# STEP 3: Test your model in terminal by running django shell


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'id', 'due_date', 'is_completed', 'list')
