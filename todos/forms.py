from django.forms import ModelForm
from todos.models import TodoList, TodoItem
from django import forms


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
            "id",
        ]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = (
            'task',
            )
