from django.test import TestCase

# Create your tests here.


class MyTest(TestCase):
    def test_something(self):
        pass


'''
To default pass the tests.py file in Django,
you can use the pass keyword to create empty test methods.
This will create a test method called test_something that does nothing,
but will not result in an error.
You can add as many empty test methods as you like to default
pass the test file.
'''
