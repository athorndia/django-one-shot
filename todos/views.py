from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# STEP 4: Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
        }
    return render(request, 'todo_list_list.html', context)


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    todo_items = todo_list.items.all()

    context = {
        'todo_list': todo_list,
        'todo_items': todo_items
    }
    return render(request, 'todo_list_detail.html', context)


def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoListForm()
    context = {
        'form': form
    }
    return render(request, 'todo_list_create.html', context)


def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        'form': form,
        'todo_list': todo_list,
    }
    return render(request, 'todo_list_update.html', context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    context = {
        'todo_list': todo_list
    }
    return render(request, 'todo_list_delete.html', context)


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            todo_list_id = request.POST.get('list')
            todo_list = TodoList.objects.get(id=todo_list_id)
            item.list = todo_list
            item.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        'form': form,
        "todo_lists": TodoList.objects.all()
    }
    return render(request, 'todo_item_create.html', context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    form = TodoItemForm(request.POST or None, instance=item)
    if form.is_valid():
        item = form.save(commit=False)
        todo_list_id = request.POST.get('list')
        todo_list = TodoList.objects.get(id=todo_list_id)
        item.list = todo_list
        item.save()
        form.save()
        success = True
        return redirect('todo_list_detail', id=item.list.id)
    else:
        success = False
    context = {
        'form': form,
        'success': success,
        "todo_lists": TodoList.objects.all()
    }
    return render(request, 'todo_item_update.html', context)
